//
//  ViewController.swift
//  MathFunctionsInSwift
//
//  Created by k ely on 2/26/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let variance: Double = varianceOf(scores: scoresArray)
    println("VARIANCE: \(variance) ")
    let median: Double = medianOf(scores: scoresArray)
    println("MEDIAN: \(median) ")
    let average: Double = averageOf(scores: scoresArray)
    println("AVERAGE: \(average) ")
    let area: CGFloat = areaOfIrregularPolygonFrom(points: polygon)
    println("AREA OF IRREGULAR POLYGON: \(area) ")
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}


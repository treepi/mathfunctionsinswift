//
//  Utility.swift
//  MathFunctionsInSwift
//
//  Created by k ely on 2/26/15.
//  Copyright (c) 2015 flyingfresh. All rights reserved.
//

import Foundation
import UIKit

let point1 = CGPoint(x: 2.66, y: 4.71)
let point2 = CGPoint(x: 5, y: 3.5)
let point3 = CGPoint(x: 3.63, y: 2.52)
let point4 = CGPoint(x: 4, y: 1.6)
let point5 = CGPoint(x: 1.9, y: 1)
let point6 = CGPoint(x: 0.72, y: 2.28)
let polygon = [point1, point2, point3, point4, point5, point6]
let scoresArray: [Double] = [17.0, 15.0, 18.0, 1.0, 9.0, 13.0]

func areaOfIrregularPolygonFrom(#points: [CGPoint])
  -> CGFloat {
    
  let numberOfSides = points.count
  var leftSum: CGFloat = 0
  var rightSum: CGFloat = 0
  var tempPoints: [CGPoint] = points
  tempPoints.append(points.first!)
  
  for var i = 1; i < tempPoints.count; i++ {
    leftSum += tempPoints[i-1].x * tempPoints[i].y
    rightSum += tempPoints[i-1].y * tempPoints[i].x
  }
  let area = (leftSum - rightSum) / 2
  return area > 0 ? area : -area
}

func varianceOf(#scores: [Double]) -> Double {
  var average = 0.0
  for score in scores {
    average += score
  }
  average /= Double(scores.count)
  
  var zigma = 0.0
  for score in scores {
    var sum = score - average
    sum *= sum
    zigma += sum
  }
  let variance = zigma / Double(scores.count - 1)
  return variance
}

func medianOf(#scores: [Double]) -> Double {
  var sortedScores = sorted(scores) { $0 < $1 }
  println(sortedScores)
  if scores.count % 2 == 0 {
    let median = (sortedScores[scores.count/2]
      + sortedScores[(scores.count/2) - 1]) / 2
    return median
  }
  else {
    let median = sortedScores[scores.count/2]
    return median
  }
}

func averageOf(#scores: [Double]) -> Double {
  var average = 0.0
  for score in scores {
    average += score
  }
  average /= Double(scores.count)
  return average
}

func fibonacci(number: Int) -> Double {
  return
    number < 2 ? Double(number) :
    fibonacci(number - 1) + fibonacci(number - 2)
}
